#!/bin/bash

####################
### DESCRIPTION ###
####################

# Script that helps to manage the hashes you get as you PASS each level for Bandit Wargame (OverTheWire). It falicitates to go to previous levels and to save the hashes.
# If you want to use this script for other wargame just modify the user variables.

######################
### USER VARIABLES ###
######################

levels=34 # Total levels in the wargame
port=2220 # SSH port
wargame=bandit # wargame name
ssh_command='sshpass -v -p $PASS ssh $wargame$LEVEL@$wargame.labs.overthewire.org -p $port'

#################
### CONSTANTS ###
#################

PASS_FILE="passwords.txt"

##### Colors #####
YELLOW='\033[1;33m'
PURPLE='\033[0;35m'
LIGHT_PURPLE='\033[1;35m'
CYAN='\033[0;36m'
LIGHT_CYAN='\033[1;36m'
RED='\033[0;31m'
LIGHT_RED='\033[1;32m'
BLUE='\033[0;34m'
TURQUOISE='\033[0;36m'
GRAY='\033[0;37m'
DARK_GRAY='\033[1;30m'
NO_COLOR='\033[0m'

HEADER="
\t________                   ___________.__           __      __.__                
\t\_____  \___  __ __________\__    ___/|  |__   ____/  \    /  \__|______   ____  
\t /   |   \  \/ // __ \_  __ \|    |   |  |  \_/ __ \   \/\/   /  \_  __ \_/ __ \ 
\t/    |    \   /\  ___/|  | \/|    |   |   Y  \  ___/\        /|  ||  | \/\  ___/ 
\t\_______  /\_/  \___  >__|   |____|   |___|  /\___  >\__/\  / |__||__|    \___  >
\t        \/          \/                     \/     \/      \/                  \/ 
"                                                              
                                                              


MENU="
\t${YELLOW}[1]${NO_COLOR} ${GRAY}Make SSH connection${NO_COLOR}\n
\t${YELLOW}[2]${NO_COLOR} ${GRAY}Delete last stored connection${NO_COLOR}\n
\t${YELLOW}[3]${NO_COLOR} ${GRAY}Show passwords file${NO_COLOR}\n
\t${YELLOW}[4]${NO_COLOR} ${GRAY}Copy the SSH command to clipboard${NO_COLOR}\n\n
"

# Script messages
MSG_INPUT_CHOICE="\t${PURPLE}[*]${NO_COLOR}${GRAY} Choice [default 1]: ${NO_COLOR}"
MSG_INPUT_LEVEL="\t${PURPLE}▶${NO_COLOR} ${GRAY}Level: ${NO_COLOR}"
MSG_INPUT_PASSWORD="\t${PURPLE}▶${NO_COLOR} ${GRAY}Hash: ${NO_COLOR}"
INFO_CHOICE='${LIGHT_PURPLE}You have selected choice${NO_COLOR} ${YELLOW}[$CHOICE]${NO_COLOR}'
INFO_PRESS_KEY="\n\t${GRAY}---------------------- Press any key to exit -------------------${NO_COLOR}"
INFO_DELETE_SUCCESS="\t${BLUE}[i] Successfully deleted!${NO_COLOR}"
INFO_EMPTY_FILE="\t${BLUE}[i] File is empty!${NO_COLOR}"
INFO_PASSWORD_FOUND='${BLUE}[i]${NO_COLOR} ${GRAY}Password found! Using $PASS ${NO_COLOR}'
INFO_DONT_EXIT="${BLUE}[i] Don't exit the script or you'll lose it!${NO_COLOR}"
ERR_WRONG_PASS="\n\t${RED}[!] Check if it's the right password.${NO_COLOR}"
ERR_INVALID_LEVEL="\n\t${RED}[!] Please type a valid level.${NO_COLOR}"
ERR_EMPTY_HASH="\t${RED}[!] Please, enter a hash.${NO_COLOR}"

###################
### FUNCTIONS #####
###################

# Checks if exists the file where the hashes will be stored
check-file() {
  if [ ! -f $PASS_FILE ]; then # if file does not exist then, it creates it
    touch $PASS_FILE
  fi
}

# Connects to the specified wargame level
connect-ssh() {
   if ! eval $ssh_command 2>stdout-ssh.txt; then # sshpass exit status is always 0, it wouldn't be like that if only ssh command is used
     if fingerprint=$(grep "key fingerprint" stdout-ssh.txt); then # If host key fingerprint is not in known_hosts

      while true; do # It loops until it gets a "yes" or "no" as an answer
        echo -ne "\tYou have to add host key fingerprint to ~/.ssh/known_hosts to continue.\n\t$fingerprint\n\tDo you want to add it? [yes/no] " && read CONSENT
        case $CONSENT in
          yes)
            ssh-keyscan -t "ed25519" -p $port $wargame.labs.overthewire.org 2>/dev/null >> ~/.ssh/known_hosts && echo -e "\tAdding fingerprint to ~/.ssh/known_hosts..."
            sleep 2
            return 1
            ;;
          no)
            echo -e "\tYou can't connect to host if you don't add the key fingerprint..."
            sleep 2
            return 1
            ;;
          *)
            echo -e "\tPlease, enter \"yes\" or \"no\""
            sleep 2
            ;;
        esac
        clear
      done

    elif grep "Permission denied" stdout-ssh.txt >/dev/null; then # If wrong password
      echo -e $ERR_WRONG_PASS && sleep 5
      return 1

    fi
   fi
}

# Traps ctrl-c to exit the script
trap ctrl_c INT
function ctrl_c() {
        # Do some cleanup before exiting
        if [ -f stoud-ssh.txt ]; then # if file does not exist then, it creates it
          rm stdout-ssh.txt
        fi
        
        echo -e "\n\n ${GRAY}Bye!${NO_COLOR}"
        exit 0
}

###################
##### MAIN ########
###################

check-file

clear # clear screen

while true; do

  echo -e "${TURQUOISE}$HEADER${NO_COLOR}"
  echo -e $MENU

  # Wait for user to input CHOICE
  echo -ne $MSG_INPUT_CHOICE && read CHOICE

  # Shell parameter expansion to set default value to 1 if there isn't user input
  CHOICE=${CHOICE:-1}

  clear # clear screen

  echo -e "${TURQUOISE}$HEADER${NO_COLOR}"
  echo -e $MENU

  # Information about option chosen
  echo -ne "\t"
  eval echo -e $INFO_CHOICE

  if [ $CHOICE == 1 ]; then

   # Wait for user to input LEVEL
   echo -ne $MSG_INPUT_LEVEL && read LEVEL

   if ([ -n $LEVEL ] && [ $LEVEL -le $levels ]) 2>/dev/null; then # checks if LEVEL is not empty and if it's a valid level for the specified wargame

      if (grep -qE "^$LEVEL" $PASS_FILE); then # If LEVEL is found in PASS_FILE
        # Grab the level password from PASS_FILE
        PASS=$(grep -E "^$LEVEL" $PASS_FILE | awk '{print $2}')

        echo -ne "\n\t"
        eval echo -e $INFO_PASSWORD_FOUND
        sleep 1
        connect-ssh

      else
        # Wait for user to input PASS
        echo -ne $MSG_INPUT_PASSWORD && read PASS # asks for hash

        if [ -z $PASS ]; then # If PASS is empty
          echo -e $ERR_EMPTY_HASH
          sleep 1
        else
          # It stores the password only if connect-ssh function is successful
          connect-ssh && echo $LEVEL $PASS >> $PASS_FILE
        fi
      fi

   else # if LEVEL is empty
    echo -e $ERR_INVALID_LEVEL
    sleep 1
   fi
 elif [ $CHOICE == 2 ]; then
   if [ -s $PASS_FILE ]; then # if file exists and it is not empty
    cp $PASS_FILE $PASS_FILE.tmp
    head -n -1 $PASS_FILE.tmp > $PASS_FILE
    rm -f $PASS_FILE.tmp
    echo -e $INFO_DELETE_SUCCESS
    sleep 1
  else
    echo -e $INFO_EMPTY_FILE
    sleep 1
  fi
 elif [ $CHOICE == 3 ]; then
   if [ -s $PASS_FILE ]; then # if file exists and it is not empty
     echo -e "\n\n${CYAN}"
     cat $PASS_FILE
     echo -e "${NO_COLOR}"
     echo -e $INFO_PRESS_KEY
     read -n 1
   else
     echo -e $INFO_EMPTY_FILE
     sleep 1
   fi
 elif [ $CHOICE == 4 ]; then
   echo -ne $MSG_INPUT_LEVEL && read LEVEL # asks for input

   if ([ -n $LEVEL ] && [ $LEVEL -le $levels ]) 2>/dev/null; then # checks if LEVEL is a number and if it is valid
    if (grep -qE "^$LEVEL" $PASS_FILE); then # If $LEVEL found in passwords.txt
      # Grab password from file
      PASS=$(grep -E "^$LEVEL" $PASS_FILE | awk '{print $2}')
      echo -e $INFO_PASSWORD_FOUND
    else
      # Wait for user to input PASS
      echo -ne $MSG_INPUT_PASSWORD && read PASS
      echo -e "\n" # echo -ne "" ??
    fi

    if [ -z $PASS ]; then # If PASS is empty # It has to check PASS any way so..... it should work
      echo -e $ERR_EMPTY_HASH
      sleep 1
    else
      # Copy command to clipboard
      eval echo -e $ssh_command | tr -d '\n' | xclip -selection clipboard
      eval echo -e $ssh_command | tr -d '\n' | xclip

      # Info messages
      echo -ne "\n Copied '"
      eval echo -ne $ssh_command
      echo -e "' to clipboard!"
      echo -e $INFO_DONT_EXIT
      echo -e $INFO_PRESS_KEY

      # Wait until it reads any character
      read -n 1
    fi
  else
    echo -e $ERR_INVALID_LEVEL
    sleep 1
  fi
 fi
  clear # clean screen
done
