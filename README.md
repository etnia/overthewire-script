# Bandit (OverTheWire) Script

![](Images/script-menu.png)

Script that helps to manage the SSH connections and the hashes you get as you pass each level. It falicitates to go to previous levels and to save the hashes.

It is created for [Bandit](https://overthewire.org/wargames/bandit/) wargame specifically although it can be adapted to any [OverTheWire](https://overthewire.org/wargames/) wargame just by modifying the **user variables** ([here](https://gitlab.com/etnia/overthewire-script/-/blob/main/overthewire.sh#L16)).
